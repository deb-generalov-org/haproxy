#!/bin/bash -xe

echo 'deb [trusted=yes] http://deb.generalov.org/debian ./buster/' > /etc/apt/sources.list.d/deb.generalov.org.list
apt-get update -q
apt-cache search haproxy
apt-get install -yq haproxy

mkdir /etc/haproxy
cat << EOF > /etc/haproxy/haproxy.cfg
global
  maxconn 122880

defaults
  mode http  
  timeout connect 5s
  timeout client 10s
  timeout server 120s
  timeout client-fin 1s
  timeout server-fin 1s
  timeout http-request 10s
  timeout http-keep-alive 50s

frontend stats
  bind *:8404
  mode http
  option http-use-htx
  http-request use-service prometheus-exporter if { path /metrics }
  stats enable
  stats uri /stats
  stats refresh 10s
EOF

/usr/local/sbin/haproxy -f /etc/haproxy/haproxy.cfg -p /run/haproxy.pid -q -S /run/haproxy-master.sock


