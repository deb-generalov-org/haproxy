#!/bin/bash -xe

[ -z "${VERSION}" ] && (echo "VERSION empty" && exit 1)
[ -z "${RELEASE}" ] && (echo "RELEASE empty" && exit 1)

cat << EOF > /etc/apt/sources.list
deb http://deb.debian.org/debian buster main contrib non-free
deb http://security.debian.org/debian-security buster/updates main
deb http://deb.debian.org/debian buster-updates main contrib non-free
deb http://deb.debian.org/debian buster-backports main contrib non-free
EOF

apt-get update -q
apt-get install -yq git ca-certificates gcc libc6-dev liblua5.3-dev libpcre3-dev libssl-dev libsystemd-dev libpcre2-dev make wget zlib1g-dev checkinstall curl
git clone https://github.com/haproxy/haproxy.git -b "v${VERSION}"
cd haproxy
export CFLAGS="-O2 -g -O2 -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -fno-strict-aliasing -Wdeclaration-after-statement -fwrapv -Wno-format-truncation -Wno-null-dereference -Wno-unused-label"

make TARGET=linux-glibc USE_SYSTEMD=1 USE_GETADDRINFO=1 USE_ZLIB=1 USE_REGPARM=1 USE_OPENSSL=1 USE_LUA=1 USE_PCRE2=1 USE_PCRE2_JIT=1 USE_NS=1 EXTRA_OBJS="contrib/prometheus-exporter/service-prometheus.o"

cd contrib/systemd/
make haproxy.service
echo -en "install:\n\tinstall -v -m 644 haproxy.service /etc/systemd/system/haproxy.service" >> Makefile
checkinstall \
--default \
--type=debian \
--addso=yes \
--pkgname=haproxy-systemd \
--pkgversion=${VERSION} \
--pkgrelease=${RELEASE} \
--pkglicense="HAPROXY" \
--pkgsource=https://github.com/haproxy/haproxy.git \
--maintainer="eduard@generalov.net" \
--install=no \
--backup=no \
--nodoc

cd -
mkdir -p /usr/local/doc

checkinstall \
--default \
--type=debian \
--addso=yes \
--pkgname=haproxy \
--pkgversion=${VERSION} \
--pkgrelease=${RELEASE} \
--pkglicense="HAPROXY" \
--requires="dpkg \(\>= 1.17.14\),haproxy-systemd \(\>= ${VERSION}\),openssl,liblua5.3-0,libpcre2-8-0,libpcre2-posix0" \
--pkgsource=https://github.com/haproxy/haproxy.git \
--maintainer="eduard@generalov.net" \
--install=no \
--backup=no \
--nodoc

curl -T contrib/systemd/haproxy-systemd_${VERSION}-${RELEASE}_amd64.deb http://${AUTH}@deb.generalov.org/webdav/debian/${DEBIAN_RELEASE}/haproxy/haproxy-systemd_${VERSION}-${RELEASE}_amd64.deb
curl -T haproxy_${VERSION}-${RELEASE}_amd64.deb http://${AUTH}@deb.generalov.org/webdav/debian/${DEBIAN_RELEASE}/haproxy/haproxy_${VERSION}-${RELEASE}_amd64.deb

dpkg-scanpackages . /dev/null
